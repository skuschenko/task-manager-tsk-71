package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.exception.system.ConnectionFailedException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Session;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class ConnectionService implements IConnectionService {

    @NotNull
    private static final String LITE_MEMBER =
            "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private Map<String, String> getConnectionProperty() {
        @NotNull final Map<String, String> properties =
                new HashMap<String, String>();
        @Nullable final String driver = propertyService.getJdbcDriver();
        Optional.ofNullable(driver).orElseThrow(() ->
                new ConnectionFailedException("Driver")
        );
        @Nullable final String userName = propertyService.getJdbcUserName();
        Optional.ofNullable(userName).orElseThrow(() ->
                new ConnectionFailedException("UserName")
        );
        @Nullable final String password = propertyService.getJdbcPassword();
        Optional.ofNullable(password).orElseThrow(() ->
                new ConnectionFailedException("Password")
        );
        @Nullable final String url = propertyService.getJdbcUrl();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Url")
        );
        @Nullable final String dialect = propertyService.getJdbcDialect();
        Optional.ofNullable(dialect).orElseThrow(() ->
                new ConnectionFailedException("Dialect")
        );
        @Nullable final String hbm2ddl = propertyService.getJdbcHbm2ddl();
        Optional.ofNullable(hbm2ddl).orElseThrow(() ->
                new ConnectionFailedException("Hbm2ddl")
        );
        @Nullable final String showSql = propertyService.getJdbcShowSql();
        Optional.ofNullable(showSql).orElseThrow(() ->
                new ConnectionFailedException("ShowSql")
        );
        @Nullable final String secondLevel = propertyService.getSecondLevel();
        Optional.ofNullable(secondLevel).orElseThrow(() ->
                new ConnectionFailedException("SecondLevel")
        );
        @Nullable final String queryCache = propertyService.getQueryCache();
        Optional.ofNullable(queryCache).orElseThrow(() ->
                new ConnectionFailedException("QueryCache")
        );
        @Nullable final String minimalPuts = propertyService.getMinimalPuts();
        Optional.ofNullable(minimalPuts).orElseThrow(() ->
                new ConnectionFailedException("MinimalPuts")
        );
        @Nullable final String liteMember = propertyService.getLiteMember();
        Optional.ofNullable(liteMember).orElseThrow(() ->
                new ConnectionFailedException("LiteMember")
        );
        @Nullable final String regionPrefix = propertyService.getRegionPrefix();
        Optional.ofNullable(regionPrefix).orElseThrow(() ->
                new ConnectionFailedException("RegionPrefix")
        );
        @Nullable final String providerConfiguration =
                propertyService.getProviderConfiguration();
        Optional.ofNullable(providerConfiguration).orElseThrow(() ->
                new ConnectionFailedException("ProviderConfiguration")
        );
        @Nullable final String factoryClass =
                propertyService.getFactoryClass();
        Optional.ofNullable(factoryClass).orElseThrow(() ->
                new ConnectionFailedException("FactoryClass")
        );
        properties.put("driver", driver);
        properties.put("userName", userName);
        properties.put("password", password);
        properties.put("url", url);
        properties.put("hbm2ddl", hbm2ddl);
        properties.put("dialect", dialect);
        properties.put("showSql", showSql);
        properties.put("secondLevel", secondLevel);
        properties.put("queryCache", queryCache);
        properties.put("minimalPuts", minimalPuts);
        properties.put("liteMember", liteMember);
        properties.put("regionPrefix", regionPrefix);
        properties.put("providerConfiguration", providerConfiguration);
        properties.put("factoryClass", factoryClass);
        return properties;
    }

    @Override
    @NotNull
    public EntityManagerFactory getHibernateSqlFactory() {
        @NotNull final Map<String, String> properties = getConnectionProperty();
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(
                org.hibernate.cfg.Environment.DRIVER, properties.get("driver")
        );
        settings.put(
                org.hibernate.cfg.Environment.URL, properties.get("url"))
        ;
        settings.put(
                org.hibernate.cfg.Environment.USER, properties.get("userName")
        );
        settings.put(
                org.hibernate.cfg.Environment.PASS, properties.get("password")
        );
        settings.put(
                org.hibernate.cfg.Environment.DIALECT,
                properties.get("dialect")
        );
        settings.put(
                org.hibernate.cfg.Environment.HBM2DDL_AUTO,
                properties.get("hbm2ddl")
        );
        settings.put(
                org.hibernate.cfg.Environment.SHOW_SQL,
                properties.get("showSql")
        );
        settings.put(
                org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE,
                properties.get("secondLevel")
        );
        settings.put(
                org.hibernate.cfg.Environment.USE_QUERY_CACHE,
                properties.get("queryCache")
        );
        settings.put(
                org.hibernate.cfg.Environment.USE_MINIMAL_PUTS,
                properties.get("minimalPuts")
        );
        settings.put(
                org.hibernate.cfg.Environment.CACHE_REGION_PREFIX,
                properties.get("regionPrefix")
        );
        settings.put(
                org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG,
                properties.get("providerConfiguration")
        );
        settings.put(
                org.hibernate.cfg.Environment.CACHE_REGION_FACTORY,
                properties.get("factoryClass")
        );
        settings.put(LITE_MEMBER, properties.get("liteMember")
        );
        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry =
                registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
