package com.tsc.skuschenko.tm.service.dto;

import com.tsc.skuschenko.tm.dto.AbstractEntityDTO;
import com.tsc.skuschenko.tm.service.AbstractService;

public abstract class AbstractDTOBusinessService<E extends AbstractEntityDTO>
        extends AbstractService {
}
