package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SessionRepository extends AbstractRepository<Session> {

    @Query("SELECT e FROM SessionDTO e WHERE e.userId = :userId")
    @Nullable List<Session> findAll(@NotNull String userId);

    @Query("SELECT e FROM SessionDTO e WHERE e.id = :id")
    @Nullable Session findSessionById(@NotNull String id);

    @Modifying
    @Query("DELETE FROM SessionDTO e WHERE e.id = :id")
    void removeSessionById(@NotNull String id);

}
