package com.tsc.skuschenko.tm.listener;

import com.tsc.skuschenko.tm.component.JmsMessageComponent;
import com.tsc.skuschenko.tm.enumerated.EntityOperationType;
import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import javax.persistence.*;

import static com.tsc.skuschenko.tm.enumerated.EntityOperationType.*;

public class EntityListener {

    @PostLoad
    public void postLoad(@NotNull Object entity) throws JMSException {
        sendMessage(entity, POST_LOAD);
    }

    @PostPersist
    public void postPersist(@NotNull Object entity) throws JMSException {
        sendMessage(entity, POST_PERSIST);
    }

    @PostRemove
    public void postRemove(@NotNull Object entity) throws JMSException {
        sendMessage(entity, POST_REMOVE);
    }

    @PostUpdate
    public void postUpdate(@NotNull Object entity) throws JMSException {
        sendMessage(entity, POST_UPDATE);
    }

    @PrePersist
    public void prePersist(@NotNull Object entity) throws JMSException {
        sendMessage(entity, PRE_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull Object entity) throws JMSException {
        sendMessage(entity, PRE_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull Object entity) throws JMSException {
        sendMessage(entity, PRE_UPDATE);
    }

    private void sendMessage(
            @NotNull final Object entity,
            @NotNull final EntityOperationType operation
    ) {
        JmsMessageComponent.getInstance().
                getBroadcastService().
                sendJmsMessageAsync(entity, operation);
    }

}
