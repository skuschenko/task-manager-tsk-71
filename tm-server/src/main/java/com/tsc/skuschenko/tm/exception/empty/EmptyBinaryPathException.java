package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class EmptyBinaryPathException extends AbstractException {

    public EmptyBinaryPathException() {
        super("Error! path to file binary is empty...");
    }

}
