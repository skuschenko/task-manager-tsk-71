package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectDTORepository
        extends AbstractDTORepository<ProjectDTO> {

    @Modifying
    @Query("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
    void clear(@NotNull String userId);

    @Modifying
    @Query("DELETE FROM ProjectDTO e")
    void clearAllProjects();

    @Query("SELECT e FROM ProjectDTO e")
    @Nullable List<ProjectDTO> findAllProjectDTO();

    @Query("SELECT e FROM ProjectDTO e WHERE e.userId = :userId")
    @Nullable List<ProjectDTO> findAllWithUserId(
            @Param("userId") @NotNull String userId
    );

    @Query("SELECT e FROM ProjectDTO e WHERE e.userId = :userId and e.id = :id")
    @Nullable ProjectDTO findOneById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    @Query("SELECT e FROM ProjectDTO e WHERE e.userId = :userId")
    @Nullable List<ProjectDTO> findOneByIndex(
            @Param("userId") @NotNull String userId
    );

    @Query(
            "SELECT e FROM ProjectDTO e WHERE e.userId = :userId and " +
                    "e.name = :name"
    )
    @Nullable ProjectDTO findOneByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

    @Query("SELECT e FROM ProjectDTO e WHERE e.id = :id")
    @Nullable ProjectDTO findProjectById(@Param("id") @NotNull String id);

    @Modifying
    @Query("DELETE FROM ProjectDTO e WHERE e.userId = :userId and e.id = :id")
    void removeOneById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

}