package com.tsc.skuschenko.tm.service.dto;

import com.tsc.skuschenko.tm.api.service.dto.ITaskDTOService;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.repository.dto.TaskDTORepository;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public final class TaskDTOService extends AbstractDTOBusinessService<TaskDTO>
        implements ITaskDTOService {

    @NotNull
    @Autowired
    private TaskDTORepository entityRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO add(
            @Nullable final String userId, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        entityRepository.save(task);
        return task;
    }

    @Override
    public void addAll(@Nullable final List<TaskDTO> tasks) {
        Optional.ofNullable(tasks).ifPresent(
                items -> items.forEach(
                        item -> add(
                                item.getUserId(),
                                item.getName(),
                                item.getDescription())
                )
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        entityRepository.save(task);
        return task;

    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        entityRepository.clearAllTasks();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@NotNull final String userId) {
        entityRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityRepository.save(task);
        return task;

    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO completeByName(@NotNull final String userId,
                                  @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        return entityRepository.findAllTaskDTO();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<TaskDTO> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        return entityRepository.findAllWithUserId(userId)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return entityRepository.findAllWithUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return entityRepository.findOneByIndex(userId).get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(TaskNotFoundException::new);
        entityRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(TaskNotFoundException::new);
        entityRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(TaskNotFoundException::new);
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final TaskDTO task =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final TaskDTO task = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final TaskDTO task = Optional.ofNullable(
                findOneByName(userId, name)
        ).orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final TaskDTO task = Optional.ofNullable(
                findOneById(userId, id)
        ).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        entityRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final TaskDTO task = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        entityRepository.save(task);
        return task;
    }

}