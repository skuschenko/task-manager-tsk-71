package com.tsc.skuschenko.tm.service.model;

import com.tsc.skuschenko.tm.api.service.model.IProjectService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.model.ProjectRepository;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractBusinessService<Project>
        implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project add(
            @Nullable final User user, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);
        projectRepository.save(project);
        return project;
    }

    @Override
    public void addAll(@Nullable final List<Project> projects) {
        Optional.ofNullable(projects).ifPresent(
                items -> items.forEach(
                        item -> add(
                                item.getUser(),
                                item.getName(),
                                item.getDescription())
                )
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Project project =
                Optional.ofNullable(findOneById(id))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status.getDisplayName());
        project.setStatus(status.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeStatusByIndex(
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Project project =
                Optional.ofNullable(findOneByIndex(index))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status.getDisplayName());
        project.setStatus(status.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeStatusByName(
            @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Project project =
                Optional.ofNullable(findOneByName(name))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status.getDisplayName());
        project.setStatus(status.getDisplayName());
        projectRepository.save(project);
        return project;

    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        projectRepository.clearAllProjects();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project completeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Project project =
                Optional.ofNullable(findOneById(id))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.COMPLETE.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project completeByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Project project =
                Optional.ofNullable(findOneByIndex(index))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.COMPLETE.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project completeByName(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Project project =
                Optional.ofNullable(findOneByName(name))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.COMPLETE.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) {
        Optional.ofNullable(comparator).orElse(null);
        return projectRepository.findAll()
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }


    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return projectRepository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return projectRepository.findOneByIndex().get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return projectRepository.findOneByName(name);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneById(final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Project project =
                Optional.ofNullable(projectRepository.findProjectById(id))
                        .orElseThrow(ProjectNotFoundException::new);
        projectRepository.delete(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneByIndex(final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Project project =
                Optional.ofNullable(findOneByIndex(index))
                        .orElseThrow(ProjectNotFoundException::new);
        projectRepository.delete(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneByName(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Project project =
                Optional.ofNullable(findOneByName(name))
                        .orElseThrow(ProjectNotFoundException::new);
        projectRepository.delete(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project startById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Project project = Optional.ofNullable(
                findOneById(id)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project startByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Project project = Optional.ofNullable(
                findOneByIndex(index)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project startByName(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Project project = Optional.ofNullable(
                findOneByName(name)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateOneById(
            @Nullable final String id, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Project project = Optional.ofNullable(
                findOneById(id)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateOneByIndex(
            @Nullable final Integer index, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Project project = Optional.ofNullable(
                findOneByIndex(index)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return project;
    }

}
