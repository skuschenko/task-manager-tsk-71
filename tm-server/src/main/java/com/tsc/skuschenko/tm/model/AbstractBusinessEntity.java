package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractBusinessEntity extends AbstractEntity {

    @Column
    @Nullable
    private Date created = new Date();

    @Column(name = "dateEnd")
    @Nullable
    private Date dateFinish;

    @Column(name = "dateBegin")
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private String description = "";

    @Column
    @Nullable
    private String name = "";

    @Column
    @Nullable
    private String status = Status.NOT_STARTED.getDisplayName();

    @ManyToOne
    @Nullable
    private User user;

}
