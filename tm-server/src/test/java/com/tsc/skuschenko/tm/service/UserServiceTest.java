package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.dto.IUserDTOService;
import com.tsc.skuschenko.tm.configuration.ServerConfiguration;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.service.dto.UserDTOService;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class UserServiceTest {

    @NotNull
    private static AnnotationConfigApplicationContext context;
    @NotNull
    private static IUserDTOService userService;

    @BeforeClass
    public static void beforeClass() {
        context =
                new AnnotationConfigApplicationContext(
                        ServerConfiguration.class
                );
        userService = testService();
    }

    @NotNull
    private static IUserDTOService testService() {
        @NotNull final IPropertyService propertyService =
                new PropertyService();
        @NotNull final IConnectionService connectionService =
                context.getBean(ConnectionService.class);
        Assert.assertNotNull(connectionService);
        Assert.assertNotNull(propertyService);
        @NotNull final IUserDTOService userService =
                context.getBean(UserDTOService.class);
        Assert.assertNotNull(userService);
        return userService;
    }

    @Before
    public void before() {
        userService.clear();
    }

    @Test
    public void testCreate() {
        @NotNull final UserDTO user = userService.create("user1", "user1");
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
    }

    @Test
    public void testCreateWithEmail() {
        @NotNull final UserDTO user =
                userService.create("user1", "user1", "user1@user1.ru");
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals("user1@user1.ru", user.getEmail());
    }

    @Test
    public void testCreateWithRole() {
        @NotNull final UserDTO user =
                userService.create("user1", "user1", Role.USER);
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals(Role.USER.getDisplayName(), user.getRole());
    }

    @Test
    public void testFindByEmail() {
        userService.create("user1", "user1", "user1@user1.ru");
        @Nullable final UserDTO userFind =
                userService.findByEmail("user1@user1.ru");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1@user1.ru", userFind.getEmail());
    }

    @Test
    public void testFindByLogin() {
        userService.create("user1", "user1");
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1", userFind.getLogin());
    }

    @Test
    public void testLockUserByLogin() {
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setLocked(true);
        userService.lockUserByLogin(user.getLogin());
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertTrue(userFind.isLocked());
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final UserDTO user = userService.create("user1", "user1");
        @Nullable final UserDTO userFind =
                userService.removeByLogin(user.getLogin());
        Assert.assertNull(userFind);
    }

    @Test
    public void testSetPassword() {
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setPasswordHash(HashUtil.salt("secret", 35484, "password"));
        Assert.assertEquals(
                HashUtil.salt("secret", 35484, "password"),
                user.getPasswordHash());
    }

    @Test
    public void testUnlockUserByLogin() {
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setLocked(false);
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertFalse(userFind.isLocked());
    }

    @Test
    public void testUpdateUser() {
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setFirstName("FirstName");
        user.setMiddleName("MiddleName");
        user.setLastName("LastName");
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        userService.updateUser(
                user.getId(), user.getFirstName(), user.getLastName(),
                user.getMiddleName());
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertEquals("FirstName", userFind.getFirstName());
        Assert.assertEquals("MiddleName", userFind.getMiddleName());
        Assert.assertEquals("LastName", userFind.getLastName());
    }

}
