package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.IUserRestEndpoint;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.constant.HeaderConstant;
import com.tsc.skuschenko.tm.constant.UrlConstant;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/users")
@WebService(
        endpointInterface = "com.tsc.skuschenko.tm.api.endpoint." +
                "IUserRestEndpoint"
)
public class UserEndpoint implements IUserRestEndpoint {

    @Autowired
    @NotNull
    private IUserService userService;

    @Override
    @WebMethod
    @PostMapping(
            value = UrlConstant.CREATE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void create(
            @WebParam(name = "user")
            @RequestBody @NotNull final User user
    ) {
        userService.save(user);
    }

    @Override
    @WebMethod
    @DeleteMapping(
            value = UrlConstant.DELETE_BY_LOGIN_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void deleteByLogin(
            @WebParam(name = "login")
            @PathVariable("login") @NotNull final String login
    ) {
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    @GetMapping(
            value = UrlConstant.FIND_BY_ID_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public User find(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping(
            value = UrlConstant.FIND_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Collection<User> findAll() {
        return userService.findAll();
    }

    @Override
    @WebMethod
    @PutMapping(
            value = UrlConstant.SAVE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void save(
            @WebParam(name = "user")
            @RequestBody @NotNull final User user
    ) {
        userService.save(user);
    }

}
