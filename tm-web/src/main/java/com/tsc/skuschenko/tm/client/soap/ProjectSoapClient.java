package com.tsc.skuschenko.tm.client.soap;

import com.tsc.skuschenko.tm.api.endpoint.IProjectRestEndpoint;
import org.jetbrains.annotations.NotNull;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ProjectSoapClient {

    public static IProjectRestEndpoint getInstance(
            @NotNull final String baseURL
    ) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/ProjectEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "ProjectEndpointService";
        @NotNull final String ns = "http://endpoint.tm.skuschenko.tsc.com/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IProjectRestEndpoint soap =
                Service.create(url, name).getPort(IProjectRestEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) soap;
        bindingProvider.getRequestContext()
                .put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return soap;
    }

}
