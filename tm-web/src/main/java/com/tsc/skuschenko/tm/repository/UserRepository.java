package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.model.Role;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Modifying
    @Query("DELETE FROM User e")
    void clear();

    @Query("SELECT e FROM User e ")
    @Nullable List<User> findAllUser();

    @Query(value = "SELECT * FROM tm_role ",nativeQuery = true)
    @NotNull Object[] findAllRoles();

    @Query("SELECT e FROM User e WHERE e.email = :email")
    @Nullable User findByEmail(@Param("email") @NotNull String email);

    @Query("SELECT e FROM User e WHERE e.login = :login")
    @Nullable User findByLogin(@Param("login") @NotNull String login);

    @Query("SELECT e FROM User e WHERE e.id = :id")
    @Nullable User findUserById(@Param("id") @NotNull String id);

    @Modifying
    @Query("UPDATE User e SET e.isLocked=true WHERE e.login = :login")
    @NotNull User lockUserByLogin(@Param("login") @NotNull String login);

    @Modifying
    @Query("DELETE FROM User e WHERE e.login = :login ")
    void removeByLogin(@Param("login") @NotNull String login);

    @Modifying
    @Query("DELETE FROM User e WHERE e.id = :id")
    void removeOneById(@Param("id") @NotNull String id);

    @Modifying
    @Query(
            "UPDATE User e SET e.passwordHash = :password WHERE " +
                    "e.id = :userId"
    )
    @NotNull User setPassword(
            @Param("userId") @NotNull String userId,
            @Param("password") @NotNull String password);

    @Modifying
    @Query("UPDATE User e SET e.isLocked=false WHERE e.login = :login")
    @NotNull User unlockUserByLogin(@Param("login") @NotNull String login);

}
