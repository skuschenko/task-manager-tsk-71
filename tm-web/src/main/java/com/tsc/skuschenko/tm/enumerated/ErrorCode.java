package com.tsc.skuschenko.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum ErrorCode {

    ERROR("Error"),
    OK("Ok");

    @NotNull
    private final String displayName;

}
