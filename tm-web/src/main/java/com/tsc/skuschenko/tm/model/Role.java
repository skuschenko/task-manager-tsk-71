package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.RoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_role")
public class Role {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public String toString() {
        return roleType.name();
    }

}