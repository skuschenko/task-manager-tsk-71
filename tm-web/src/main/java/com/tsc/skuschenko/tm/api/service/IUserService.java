package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.RoleType;
import com.tsc.skuschenko.tm.model.Role;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService {

    void addAll(@Nullable List<User> users);

    void clear();

    @NotNull
    Object[] findAllRoles();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable RoleType roleType
    );

    @Nullable
    @SneakyThrows
    List<User> findAll();

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    boolean isEmailExist(@Nullable String login);

    boolean isLoginExist(@Nullable String login);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

    @NotNull
    User save(@Nullable User user);

    @NotNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}
