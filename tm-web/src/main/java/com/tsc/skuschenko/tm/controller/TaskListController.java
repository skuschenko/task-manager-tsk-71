package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TaskListController {

    @Autowired
    @NotNull
    private ITaskService taskService;

    @Autowired
    @NotNull
    private IUserService userService;

    @GetMapping("/tasks")
    @NotNull
    public ModelAndView index() {
        return new ModelAndView(
                "task-list", "tasks",
                taskService.findAllByUserId(UserUtil.getUserId())
        );
    }

}
