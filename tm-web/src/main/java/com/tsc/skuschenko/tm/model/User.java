package com.tsc.skuschenko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_user")
public class User extends AbstractEntity {

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column(name = "locked")
    private boolean isLocked = false;

    @Column
    @Nullable
    private String lastName;

    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    private String passwordHash;

    @OneToMany(
            mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Nullable
    private List<Role> roles;

}
