package com.tsc.skuschenko.tm.client.soap;

import com.tsc.skuschenko.tm.api.endpoint.IUserRestEndpoint;
import org.jetbrains.annotations.NotNull;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class UserSoapClient {

    public static IUserRestEndpoint getInstance(@NotNull final String baseURL)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/UserEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "UserEndpointService";
        @NotNull final String ns = "http://endpoint.tm.skuschenko.tsc.com/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IUserRestEndpoint soap =
                Service.create(url, name).getPort(IUserRestEndpoint.class);
        return soap;
    }

}
