package com.tsc.skuschenko.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum RoleType {

    ADMINISTRATOR("Administrator"),
    USER("User");

    @NotNull
    private final String displayName;

}