package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    private static final String USER_ID = "qwerty";

    @Autowired
    ProjectRepository projectRepository;

    @Before
    public void before() {
        projectRepository.clearAll();
        @NotNull final Project project = new Project("pro");
        project.setUserId(USER_ID);
        projectRepository.save(project);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        @NotNull final Project project = new Project("pro");
        project.setUserId(USER_ID);
        projectRepository.save(project);
        @Nullable final Project projectNew =
                projectRepository.findProjectById(USER_ID, project.getId());
        Assert.assertNotNull(projectNew);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void createAll() {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Project project = new Project("pro" + i);
            project.setUserId(USER_ID);
            projects.add(project);
            projectRepository.save(project);
        }
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteAll() {
        projectRepository.clearAllByUserId(USER_ID);
        @Nullable final List<Project> projectNew = findAllProjects();
        Assert.assertEquals(0, projectNew.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteById() {
        @NotNull final Project project = findAllProjects().get(0);
        projectRepository.removeById(USER_ID, project.getId());
        @Nullable final Project projectFind =
                projectRepository.findProjectById(USER_ID, project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void findAll() {
        findAllProjects();
    }
    @NotNull
    private List<Project> findAllProjects() {
        @Nullable final Collection<Project> projects =
                projectRepository.findAll();
        if (!Optional.ofNullable(projects).isPresent())
            return new ArrayList<>();
        return new ArrayList<>(projects);
    }


    @Test
    @Category(UnitWebCategory.class)
    public void findById() {
        @NotNull final List<Project> projects = findAllProjects();
        @Nullable final Project projectFind =
                projectRepository.findProjectById(USER_ID, projects.get(0).getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void save() {
        @NotNull final Project project = findAllProjects().get(0);
        project.setDescription(UUID.randomUUID().toString());
        projectRepository.save(project);
        Assert.assertEquals(
                project.getDescription(),
                findAllProjects().get(0).getDescription()
        );
    }

    @Test
    @Category(UnitWebCategory.class)
    public void saveAll() {
        @NotNull final List<Project> projects = findAllProjects();
        projects.forEach(item -> {
                    item.setDescription(UUID.randomUUID().toString());
                    projectRepository.save(item);
                }
        );
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

}