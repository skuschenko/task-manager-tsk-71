package com.tsc.skuschenko.tm.endpoint.rest;

import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRestEndpointTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/api/users/";

    @NotNull
    private static final String AUTH_URL = "http://localhost:8080/api/auth/";

    @NotNull
    private static final String CREATE_METHOD = "create";

    @NotNull
    private static final String DELETE_BY_LOGIN_METHOD = "deleteByLogin/";

    @NotNull
    private static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    private static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    private static final String JSESSIONID = "JSESSIONID";

    @NotNull
    private static final String LOGIN = "login";

    @NotNull
    private static final String LOGOUT = "logout";

    @NotNull
    private static final String SAVE_METHOD = "save";

    @Nullable
    private static String USER_ID = null;

    @Nullable
    private static String sessionId = null;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void auth() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                AUTH_URL + LOGIN + "?username=test&password=test";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add(JSESSIONID + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + LOGOUT;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<Result> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                Result.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        sessionId = null;
    }

    @Before
    public void before() {
        create();
        deleteByLogin();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull String url = API_URL + CREATE_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final User user = new User();
        user.setLogin("test1");
        user.setPasswordHash("test1");
        @NotNull final HttpEntity<User> httpEntity =
                new HttpEntity<>(user, getHeader());
        restTemplate.postForEntity(url, httpEntity, User.class);
        url = API_URL + FIND_BY_ID_METHOD + user.getId();
        @NotNull final ResponseEntity<User> response = restTemplate.exchange(
                url, HttpMethod.GET, httpEntity, User.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final User userNew = response.getBody();
        Assert.assertNotNull(userNew);
        Assert.assertEquals(user.getId(), userNew.getId());
        USER_ID = user.getId();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteByLogin() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                API_URL + DELETE_BY_LOGIN_METHOD + "test1";
        HttpHeaders headers = getHeader();
        headers.add("X-HTTP-Method-Override", "DELETE");
        @NotNull final HttpEntity httpEntity =
                new HttpEntity<>(headers);
        try {
            ResponseEntity<String> responseMS = restTemplate.exchange(
                    url, HttpMethod.DELETE, httpEntity,
                    String.class
            );
        } catch (HttpClientErrorException ex) {
            String message = ex.getResponseBodyAsString();
            System.out.println(message);
        }
        Assert.assertEquals(0, findAllUsers().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        findAllUsers();
    }

    @NotNull
    private List<User> findAllUsers() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<List<User>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<List<User>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    public User findById() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD + USER_ID;
        @NotNull final ResponseEntity<User> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                User.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findByIdTest() {
        findById();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final User user = findById();
        user.setLastName(UUID.randomUUID().toString());
        @NotNull final HttpEntity<User> httpEntity =
                new HttpEntity<>(user, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, User.class
        );
        Assert.assertEquals(
                user.getLastName(),
                findById().getLastName()
        );
    }

}
