package com.tsc.skuschenko.tm.endpoint.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.endpoint.IAuthSoapEndpoint;
import com.tsc.skuschenko.tm.api.endpoint.IProjectRestEndpoint;
import com.tsc.skuschenko.tm.client.soap.AuthSoapClient;
import com.tsc.skuschenko.tm.client.soap.ProjectSoapClient;
import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @Nullable
    private static String USER_ID =
            "7692fe68-3160-47a3-bee5-6372451820c0";

    @NotNull
    private static IAuthSoapEndpoint authEndpoint;

    @NotNull
    private static IProjectRestEndpoint endpoint;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    @SneakyThrows
    @Category(IntegrationWebCategory.class)
    public static void auth() {
        authEndpoint = AuthSoapClient.getInstance(URL);
        Assert.assertTrue(
                authEndpoint.login("test", "test").getSuccess()
        );
        endpoint = ProjectSoapClient.getInstance(URL);
        setSession();
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
        USER_ID = getProfile().getId();
    }
    @NotNull
    public static User getProfile() {
        @Nullable final User user = authEndpoint.profile();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
        return user;
    }

    public static void logout() {
        Assert.assertTrue(authEndpoint.logout().getSuccess());
    }

    @Before
    public void before() {
        deleteAll();
        create();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull final Project project = new Project("pro");
        project.setUserId(USER_ID);
        endpoint.create(project);
        @Nullable final Project projectNew = endpoint.find(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void createAll() {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Project project = new Project("pro" + i);
            project.setUserId(USER_ID);
            projects.add(project);
        }
        endpoint.createAll(projects);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteAll() {
        @NotNull final List<Project> projects = findAllProjects();
        if (projects.size()==0)return;
        endpoint.deleteAll(projects);
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteById() {
        @NotNull final Project project = findAllProjects().get(0);
        endpoint.deleteById(project.getId());
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        findAllProjects();
    }

    @NotNull
    private List<Project> findAllProjects() {
        @Nullable final Collection<Project> projects = endpoint.findAll();
        if (!Optional.ofNullable(projects).isPresent())
            return new ArrayList<>();
        return new ArrayList<>(projects);
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findById() {
        @NotNull final List<Project> projects = findAllProjects();
        Assert.assertNotNull(projects);
        Assert.assertNotNull(endpoint.find(projects.get(0).getId()));
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @NotNull final Project project = findAllProjects().get(0);
        project.setDescription(UUID.randomUUID().toString());
        endpoint.save(project);
        Assert.assertEquals(
                project.getDescription(),
                findAllProjects().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void saveAll() {
        @NotNull final List<Project> projects = findAllProjects();
        projects.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        endpoint.saveAll(projects);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    private static void setSession() throws MalformedURLException {
        @NotNull final BindingProvider bindingProvider =
                (BindingProvider) authEndpoint;
        @NotNull final ObjectMapper oMapper = new ObjectMapper();
        @NotNull final Object headers = bindingProvider.getResponseContext()
                .get(MessageContext.HTTP_RESPONSE_HEADERS);
        @NotNull final Map<String, String> mapHeaders =
                oMapper.convertValue(headers, Map.class);
        @NotNull final Object cookieValue =
                mapHeaders.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> collection = (List<String>) cookieValue;
        ((BindingProvider) endpoint).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                Collections.singletonMap(
                        "Cookie",
                        Collections.singletonList(collection.get(0))
                )
        );
    }

}
