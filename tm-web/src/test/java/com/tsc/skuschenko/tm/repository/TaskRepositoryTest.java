package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    private static final String USER_ID = "qwerty";

    @Autowired
    TaskRepository taskRepository;

    @Before
    public void before() {
        taskRepository.clearAll();
        @NotNull final Task task = new Task("pro");
        task.setUserId(USER_ID);
        taskRepository.save(task);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        @NotNull final Task task = new Task("pro");
        task.setUserId(USER_ID);
        taskRepository.save(task);
        @Nullable final Task taskNew =
                taskRepository.findTaskById(USER_ID, task.getId());
        Assert.assertNotNull(taskNew);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void createAll() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Task task = new Task("pro" + i);
            task.setUserId(USER_ID);
            tasks.add(task);
            taskRepository.save(task);
        }
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteAll() {
        taskRepository.clearAllByUserId(USER_ID);
        @Nullable final List<Task> taskNew = findAllTasks();
        Assert.assertEquals(0, taskNew.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteById() {
        @NotNull final Task task = findAllTasks().get(0);
        taskRepository.removeById(USER_ID, task.getId());
        @Nullable final Task taskFind =
                taskRepository.findTaskById(USER_ID, task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void findAll() {
        findAllTasks();
    }

    @NotNull
    private List<Task> findAllTasks() {
        @Nullable final Collection<Task> tasks =
                taskRepository.findAll();
        if (!Optional.ofNullable(tasks).isPresent())
            return new ArrayList<>();
        return new ArrayList<>(tasks);
    }


    @Test
    @Category(UnitWebCategory.class)
    public void findById() {
        @NotNull final List<Task> tasks = findAllTasks();
        @Nullable final Task taskFind =
                taskRepository.findTaskById(USER_ID, tasks.get(0).getId());
        Assert.assertNotNull(taskFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void save() {
        @NotNull final Task task = findAllTasks().get(0);
        task.setDescription(UUID.randomUUID().toString());
        taskRepository.save(task);
        Assert.assertEquals(
                task.getDescription(),
                findAllTasks().get(0).getDescription()
        );
    }

    @Test
    @Category(UnitWebCategory.class)
    public void saveAll() {
        @NotNull final List<Task> tasks = findAllTasks();
        tasks.forEach(item -> {
                    item.setDescription(UUID.randomUUID().toString());
                    taskRepository.save(item);
                }
        );
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

}