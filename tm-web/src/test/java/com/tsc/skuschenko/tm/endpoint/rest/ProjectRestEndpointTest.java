package com.tsc.skuschenko.tm.endpoint.rest;

import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectRestEndpointTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private static final String AUTH_URL = "http://localhost:8080/api/auth/";

    @NotNull
    private static final String CREATE_ALL_METHOD = "createAll";

    @NotNull
    private static final String CREATE_METHOD = "create";

    @NotNull
    private static final String DELETE_ALL_METHOD = "deleteAll";

    @NotNull
    private static final String DELETE_BY_ID_METHOD = "deleteById/";

    @NotNull
    private static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    private static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    private static final String JSESSIONID = "JSESSIONID";

    @NotNull
    private static final String LOGIN = "login";

    @NotNull
    private static final String LOGOUT = "logout";

    @NotNull
    private static final String SAVE_ALL_METHOD = "saveAll";

    @NotNull
    private static final String SAVE_METHOD = "save";

    @Nullable
    private static final String PROFILE = "profile";

    @Nullable
    private static String USER_ID =
            "7692fe68-3160-47a3-bee5-6372451820c0";

    @Nullable
    private static String sessionId = null;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void auth() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                AUTH_URL + LOGIN + "?username=test&password=test";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
        USER_ID = getProfile().getId();
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add(JSESSIONID + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public static User getProfile() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + PROFILE;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<User> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                User.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + LOGOUT;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<Result> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                Result.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        sessionId = null;
    }

    @Before
    public void before() {
        deleteAll();
        create();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull String url = API_URL + CREATE_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final Project project = new Project("pro");
        project.setUserId(USER_ID);
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(project, getHeader());
        restTemplate.postForEntity(url, httpEntity, Project.class);
        url = API_URL + FIND_BY_ID_METHOD + project.getId();
        @NotNull final ResponseEntity<Project> response = restTemplate.exchange(
                url, HttpMethod.GET, httpEntity, Project.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Project projectNew = response.getBody();
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void createAll() {
        @NotNull final String url = API_URL + CREATE_ALL_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Project project = new Project("pro" + i);
            project.setUserId(USER_ID);
            projects.add(project);
        }
        @NotNull final HttpEntity<List> httpEntity =
                new HttpEntity<>(projects, getHeader());
        restTemplate.postForObject(url, httpEntity, List.class);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final List<Project> projects = findAllProjects();
        @NotNull final String url =
                API_URL + DELETE_ALL_METHOD;
        @NotNull final HttpEntity<Project[]> httpEntity =
                new HttpEntity<>(projects.toArray(new Project[0]), getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Project[].class
        );
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteById() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final Project project = findAllProjects().get(0);
        @NotNull final String url =
                API_URL + DELETE_BY_ID_METHOD + project.getId();
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(project, getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Project.class
        );
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        findAllProjects();
    }

    @NotNull
    private List<Project> findAllProjects() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<List<Project>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<List<Project>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findById() {
        @NotNull final List<Project> projects = findAllProjects();
        Assert.assertNotNull(projects);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD +
                projects.get(0).getId();
        @NotNull final ResponseEntity<Project> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                Project.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final Project project = findAllProjects().get(0);
        project.setDescription(UUID.randomUUID().toString());
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(project, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Project.class
        );
        Assert.assertEquals(
                project.getDescription(),
                findAllProjects().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void saveAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_ALL_METHOD;
        @NotNull final List<Project> projects = findAllProjects();
        projects.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        @NotNull final HttpEntity<List<Project>> httpEntity =
                new HttpEntity<>(projects, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Project.class
        );
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

}
