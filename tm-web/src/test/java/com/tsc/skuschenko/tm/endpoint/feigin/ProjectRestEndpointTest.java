package com.tsc.skuschenko.tm.endpoint.feigin;

import com.tsc.skuschenko.tm.client.rest.AuthRestClient;
import com.tsc.skuschenko.tm.client.rest.ProjectRestClient;
import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Result;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectRestEndpointTest {

    public static void auth() {
        final Result result =
                AuthRestClient.client().login("test", "test");
        Assert.assertTrue(result.getSuccess());
    }

    @BeforeClass
    public static void beforeClass() {
        auth();

    }

    @Before
    public void before() {
        deleteAll();
        create();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull final Project project = new Project("pro");
        ProjectRestClient.client().create(project);
        @NotNull final Project projectNew =
                ProjectRestClient.client().find(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void createAll() {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            projects.add(new Project("pro" + i));
        }
        ProjectRestClient.client().createAll(projects);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteAll() {
        Collection<Project> objects = ProjectRestClient.client().findAll();
        @NotNull final List<Project> projects = findAllProjects();
        ProjectRestClient.client().deleteAll(projects);
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteById() {
        @NotNull final Project project = findAllProjects().get(0);
        ProjectRestClient.client().deleteById(project.getId());
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        findAllProjects();
    }

    @NotNull
    private List<Project> findAllProjects() {
        @Nullable final List<Project> projects =
                new ArrayList<>(ProjectRestClient.client().findAll());
        Assert.assertNotNull(projects);
        return projects;
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findById() {
        @NotNull final List<Project> projects = findAllProjects();
        Assert.assertNotNull(projects);
        @NotNull final Project projectNew =
                ProjectRestClient.client().find(projects.get(0).getId());
        Assert.assertEquals(projectNew.getId(), projects.get(0).getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @NotNull final Project project = findAllProjects().get(0);
        project.setDescription(UUID.randomUUID().toString());
        ProjectRestClient.client().save(project);
        Assert.assertEquals(
                project.getDescription(),
                findAllProjects().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void saveAll() {
        @NotNull final List<Project> projects = findAllProjects();
        projects.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        ProjectRestClient.client().saveAll(projects);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

}
