package com.tsc.skuschenko.tm.endpoint.rest;

import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskRestEndpointTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final String AUTH_URL = "http://localhost:8080/api/auth/";

    @NotNull
    private static final String CREATE_ALL_METHOD = "createAll";

    @NotNull
    private static final String CREATE_METHOD = "create";

    @NotNull
    private static final String DELETE_ALL_METHOD = "deleteAll";

    @NotNull
    private static final String DELETE_BY_ID_METHOD = "deleteById/";

    @NotNull
    private static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    private static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    private static final String JSESSIONID = "JSESSIONID";

    @NotNull
    private static final String LOGIN = "login";

    @NotNull
    private static final String LOGOUT = "logout";

    @NotNull
    private static final String SAVE_ALL_METHOD = "saveAll";

    @NotNull
    private static final String SAVE_METHOD = "save";

    @Nullable
    private static String USER_ID =
            "7692fe68-3160-47a3-bee5-6372451820c0";

    @Nullable
    private static final String PROFILE = "profile";

    @Nullable
    private static String sessionId = null;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void auth() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                AUTH_URL + LOGIN + "?username=test&password=test";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
        USER_ID = getProfile().getId();
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add(JSESSIONID + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + LOGOUT;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<Result> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                Result.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        sessionId = null;
    }

    @Before
    public void before() {
        deleteAll();
        create();
    }

    public static User getProfile() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + PROFILE;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<User> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                User.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull String url = API_URL + CREATE_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final Task task = new Task("pro");
        task.setUserId(USER_ID);
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(task, getHeader());
        restTemplate.postForEntity(url, httpEntity, Task.class);
        url = API_URL + FIND_BY_ID_METHOD + task.getId();
        @NotNull final ResponseEntity<Task> response = restTemplate.exchange(
                url, HttpMethod.GET, httpEntity, Task.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Task taskNew = response.getBody();
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void createAll() {
        @NotNull final String url = API_URL + CREATE_ALL_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Task task = new Task("pro" + i);
            task.setUserId(USER_ID);
            tasks.add(task);
        }
        @NotNull final HttpEntity<List> httpEntity =
                new HttpEntity<>(tasks, getHeader());
        restTemplate.postForObject(url, httpEntity, List.class);
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final List<Task> tasks = findAllTasks();
        @NotNull final String url =
                API_URL + DELETE_ALL_METHOD;
        @NotNull final HttpEntity<Task[]> httpEntity =
                new HttpEntity<>(tasks.toArray(new Task[0]), getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Task[].class
        );
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteById() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final Task task = findAllTasks().get(0);
        @NotNull final String url =
                API_URL + DELETE_BY_ID_METHOD + task.getId();
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(task, getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Task.class
        );
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        findAllTasks();
    }

    @NotNull
    private List<Task> findAllTasks() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<List<Task>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<List<Task>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findById() {
        @NotNull final List<Task> tasks = findAllTasks();
        Assert.assertNotNull(tasks);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD +
                tasks.get(0).getId();
        @NotNull final ResponseEntity<Task> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                Task.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final Task task = findAllTasks().get(0);
        task.setDescription(UUID.randomUUID().toString());
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(task, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Task.class
        );
        Assert.assertEquals(
                task.getDescription(),
                findAllTasks().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void saveAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_ALL_METHOD;
        @NotNull final List<Task> tasks = findAllTasks();
        tasks.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        @NotNull final HttpEntity<List<Task>> httpEntity =
                new HttpEntity<>(tasks, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Task.class
        );
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

}
