package com.tsc.skuschenko.tm.endpoint.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.endpoint.IAuthSoapEndpoint;
import com.tsc.skuschenko.tm.api.endpoint.ITaskRestEndpoint;
import com.tsc.skuschenko.tm.client.soap.AuthSoapClient;
import com.tsc.skuschenko.tm.client.soap.TaskSoapClient;
import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @Nullable
    private static String USER_ID =
            "7692fe68-3160-47a3-bee5-6372451820c0";

    @NotNull
    private static IAuthSoapEndpoint authEndpoint;

    @NotNull
    private static ITaskRestEndpoint endpoint;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    @SneakyThrows
    public static void auth() {
        authEndpoint = AuthSoapClient.getInstance(URL);
        Assert.assertTrue(
                authEndpoint.login("test", "test").getSuccess()
        );
        endpoint = TaskSoapClient.getInstance(URL);
        setSession();
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
        USER_ID = getProfile().getId();
    }

    public static User getProfile() {
        @Nullable final User user = authEndpoint.profile();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
        return user;
    }

    public static void logout() {
        Assert.assertTrue(authEndpoint.logout().getSuccess());
    }

    @Before
    public void before() {
        deleteAll();
        create();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull final Task task = new Task("pro");
        task.setUserId(USER_ID);
        endpoint.create(task);
        @Nullable final Task taskNew = endpoint.find(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void createAll() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Task task = new Task("pro" + i);
            task.setUserId(USER_ID);
            tasks.add(task);
        }
        endpoint.createAll(tasks);
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteAll() {
        @NotNull final List<Task> tasks = findAllTasks();
        if (tasks.size()==0)return;
        endpoint.deleteAll(tasks);
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteById() {
        @NotNull final Task task = findAllTasks().get(0);
        endpoint.deleteById(task.getId());
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        findAllTasks();
    }

    @NotNull
    private List<Task> findAllTasks() {
        @Nullable final Collection<Task> tasks = endpoint.findAll();
        if (!Optional.ofNullable(tasks).isPresent())
            return new ArrayList<>();
        return new ArrayList<>(tasks);
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findById() {
        @NotNull final List<Task> tasks = findAllTasks();
        Assert.assertNotNull(tasks);
        Assert.assertNotNull(endpoint.find(tasks.get(0).getId()));
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @NotNull final Task task = findAllTasks().get(0);
        task.setDescription(UUID.randomUUID().toString());
        endpoint.save(task);
        Assert.assertEquals(
                task.getDescription(),
                findAllTasks().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void saveAll() {
        @NotNull final List<Task> tasks = findAllTasks();
        tasks.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        endpoint.saveAll(tasks);
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    private static void setSession() throws MalformedURLException {
        @NotNull final BindingProvider bindingProvider =
                (BindingProvider) authEndpoint;
        @NotNull final ObjectMapper oMapper = new ObjectMapper();
        @NotNull final Object headers = bindingProvider.getResponseContext()
                .get(MessageContext.HTTP_RESPONSE_HEADERS);
        @NotNull final Map<String, String> mapHeaders =
                oMapper.convertValue(headers, Map.class);
        @NotNull final Object cookieValue =
                mapHeaders.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> collection = (List<String>) cookieValue;
        ((BindingProvider) endpoint).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                Collections.singletonMap(
                        "Cookie",
                        Collections.singletonList(collection.get(0))
                )
        );
    }

}
