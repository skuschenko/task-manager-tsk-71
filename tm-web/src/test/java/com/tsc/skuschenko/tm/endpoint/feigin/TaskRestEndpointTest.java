package com.tsc.skuschenko.tm.endpoint.feigin;

import com.tsc.skuschenko.tm.client.rest.TaskRestClient;
import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskRestEndpointTest {

    @Before
    public void before() {
        deleteAll();
        create();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull final Task task = new Task("pro");
        TaskRestClient.client().create(task);
        @NotNull final Task taskNew =
                TaskRestClient.client().find(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void createAll() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            tasks.add(new Task("pro" + i));
        }
        TaskRestClient.client().createAll(tasks);
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteAll() {
        @NotNull final List<Task> tasks = findAllTasks();
        TaskRestClient.client().deleteAll(tasks);
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteById() {
        @NotNull final Task task = findAllTasks().get(0);
        TaskRestClient.client().deleteById(task.getId());
        Assert.assertEquals(0, findAllTasks().size());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        findAllTasks();
    }

    @NotNull
    private List<Task> findAllTasks() {
        @Nullable final List<Task> tasks =
                new ArrayList<>(TaskRestClient.client().findAll());
        Assert.assertNotNull(tasks);
        return tasks;
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findById() {
        @NotNull final List<Task> tasks = findAllTasks();
        Assert.assertNotNull(tasks);
        @NotNull final Task taskNew =
                TaskRestClient.client().find(tasks.get(0).getId());
        Assert.assertEquals(taskNew.getId(), tasks.get(0).getId());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @NotNull final Task task = findAllTasks().get(0);
        task.setDescription(UUID.randomUUID().toString());
        TaskRestClient.client().save(task);
        Assert.assertEquals(
                task.getDescription(),
                findAllTasks().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void saveAll() {
        @NotNull final List<Task> tasks = findAllTasks();
        tasks.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        TaskRestClient.client().saveAll(tasks);
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

}
