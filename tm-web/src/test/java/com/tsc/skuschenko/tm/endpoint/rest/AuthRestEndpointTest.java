package com.tsc.skuschenko.tm.endpoint.rest;

import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


public class AuthRestEndpointTest {

    @NotNull
    private static final String AUTH_URL = "http://localhost:8080/api/auth/";

    @NotNull
    private static final String LOGIN = "login";

    @NotNull
    private static final String LOGOUT = "logout";

    @NotNull
    private static final String SESSION = "session";

    @Nullable
    private static final String PROFILE = "profile";

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String JSESSIONID = "JSESSIONID";

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void auth() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                AUTH_URL + LOGIN + "?username=test&password=test";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add(JSESSIONID + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void session() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + SESSION;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<String> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                String.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void profile() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + PROFILE;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<User> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                User.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
    }

    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = AUTH_URL + LOGOUT;
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<Result> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                Result.class
        );
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        sessionId = null;
    }

}
