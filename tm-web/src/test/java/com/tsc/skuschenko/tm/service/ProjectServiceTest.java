package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    private static String USER_ID;

    @Autowired
    IProjectService projectService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        projectService.clearAll();
        @NotNull final Project project = new Project("pro");
        project.setUserId(USER_ID);
        projectService.save(project);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        @NotNull final Project project = new Project("pro");
        project.setUserId(USER_ID);
        projectService.save(project);
        @Nullable final Project projectNew = projectService.findById(project.getId());
        Assert.assertNotNull(projectNew);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void createAll() {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Project project = new Project("pro" + i);
            project.setUserId(USER_ID);
            projects.add(project);
            projectService.save(project);
        }
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteAll() {
        projectService.clearAllByUserId(USER_ID);
        @Nullable final List<Project> projectNew = findAllProjects();
        Assert.assertEquals(0, projectNew.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteById() {
        @NotNull final Project project = findAllProjects().get(0);
        projectService.removeById(project.getId());
        @Nullable final Project projectFind =
                projectService.findById(project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void findAll() {
        findAllProjects();
    }

    @NotNull
    private List<Project> findAllProjects() {
        @Nullable final Collection<Project> projects = projectService.findAll();
        if (!Optional.ofNullable(projects).isPresent())
            return new ArrayList<>();
        return new ArrayList<>(projects);
    }


    @Test
    @Category(UnitWebCategory.class)
    public void findById() {
        @NotNull final List<Project> projects = findAllProjects();
        @Nullable final Project projectFind =
                projectService.findById(projects.get(0).getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void save() {
        @NotNull final Project project = findAllProjects().get(0);
        project.setDescription(UUID.randomUUID().toString());
        projectService.save(project);
        Assert.assertEquals(
                project.getDescription(),
                findAllProjects().get(0).getDescription()
        );
    }

    @Test
    @Category(UnitWebCategory.class)
    public void saveAll() {
        @NotNull final List<Project> projects = findAllProjects();
        projects.forEach(item -> {
                    item.setDescription(UUID.randomUUID().toString());
                    projectService.save(item);
                }
        );

        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

}